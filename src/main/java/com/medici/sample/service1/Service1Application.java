package com.medici.sample.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Service1Application {
	private static final String SAMPLE = null;

	public static void main(String[] args) {
		System.out.println(SAMPLE.length());
		//on more time
		System.out.println(SAMPLE.length());

		SpringApplication.run(Service1Application.class, args);

		System.out.println("Hello World");
	}

}
