#!groovy

String sonarApi(String method, String url, String params) {
    return httpRequest(url: "https://sonarcloud.io/$url?$params",
            httpMode: method, contentType: "APPLICATION_JSON",
            consoleLogResponseBody: true,
            customHeaders: [[name: "Authorization", value: "Basic NTdlMzM2NWZkNjU4ZDU3NmY3Nzc1NGQ0OTkwYTY0YWI3ODU5YzM1OTo="]]).content
}

pipeline {
    agent any
    environment {
        SONAR_ORGANIZATION_KEY = 'yeskov-bitbucket'
    }
    stages {
        stage('Test') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2:z -u root'
                    reuseNode true
                }
            }
            steps {
                sh 'printenv'
                sh 'mvn clean verify'
            }
        }
        stage('Sonar') {
            agent {
                docker {
                    image 'alpine/git:1.0.7'
                    reuseNode true
                }
            }
            steps {
                script {
                    //Get repository info
                    def url = env.GIT_URL != null ? env.GIT_URL : env.GIT_URL_1;
                    String repositoryPath = url.replaceAll("^https://bitbucket.org/", "").replaceAll(".git\$", "");
                    def repositoryInfo = readJSON text: httpRequest("https://api.bitbucket.org/2.0/repositories/${repositoryPath}").content
                    println("Fetched repository info: $repositoryInfo");


                    
                    String projectPrefix = "organization=$SONAR_ORGANIZATION_KEY&project=$repositoryInfo.name"
                    String componentPrefix = "organization=$SONAR_ORGANIZATION_KEY&component=$repositoryInfo.name"


                    //Create and setup sonar project if not exists
                    def project = readJSON text: sonarApi("GET", "api/projects/search", "organization=$SONAR_ORGANIZATION_KEY&projects=$repositoryInfo.name")
                    if (project.paging.total == 0) {
                        println("Going to create new sonar project");

                        sonarApi("POST", "api/projects/create", "$projectPrefix&name=$repositoryInfo.name")
                        sonarApi("POST", "api/project_branches/rename", "$projectPrefix&name=develop")
                        sonarApi("POST", "api/settings/set",
                                "$componentPrefix&key=sonar.branch.longLivedBranches.regex&&value=develop%7Cmaster")
                        sonarApi("POST", "api/settings/set", "$componentPrefix&key=sonar.leak.period&value=previous_version")
                        sonarApi("POST", "api/settings/set", "$componentPrefix&key=sonar.pullrequest.provider&value=BitbucketCloud")
                    }


                    String sonarRequest = "mvn -X sonar:sonar" +
                             " -Dsonar.projectKey=$repositoryInfo.name " +
                             " -Dsonar.organization=$SONAR_ORGANIZATION_KEY " +
                             " -Dsonar.host.url=https://sonarcloud.io " +
                             " -Dsonar.login=f392c27b390ae134c99486fa416f0bc7cde72310" +
                             " -Dsonar.junit.reportPaths=$WORKSPACE/target/surefire-reports" +
                             " -Dsonar.log.level=DEBUG" +
                             " -Dsonar.verbose=true"

                    String targetBranch = null;

                    if (env.CHANGE_ID != null) {
                        sonarRequest =  sonarRequest +
                                 " -Dsonar.pullrequest.key=$env.CHANGE_ID" +
                                 " -Dsonar.pullrequest.branch=$env.CHANGE_BRANCH" +
                                 " -Dsonar.pullrequest.base=$env.CHANGE_TARGET" +
                                 " \"-Dsonar.pullrequest.bitbucketcloud.repository=$repositoryInfo.uuid\"" +
                                 " \"-Dsonar.pullrequest.bitbucketcloud.owner=$repositoryInfo.owner.uuid\""
                        targetBranch = env.CHANGE_TARGET;
                    } else {
                        if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                            sonarRequest = sonarRequest + " -Dsonar.branch.name=$env.BRANCH_NAME"
                        } else {
                            targetBranch = env.BRANCH_NAME.startsWith("release") || env.BRANCH_NAME.startsWith("hotfix") ? "master" : "develop";
                            sonarRequest = sonarRequest + " -Dsonar.branch.name=$env.BRANCH_NAME" +
                                    " -Dsonar.branch.target=$targetBranch"

                        }
                    }

                    if (targetBranch != null) {
                        sh ("git fetch --no-tags $url +refs/heads/$targetBranch:refs/remotes/origin/$targetBranch")
                    }


                    sh "cat  $WORKSPACE/target/jacoco.exec"

                    echo "Current revision:"
                    sh "git rev-parse HEAD"
                    echo "Parent revision:"
                    sh "git log --pretty=%P -n 1 \"HEAD\""


                    sh sonarRequest;
                }
            }
        }
        stage('Build') {
            steps {
                sh 'mvn clean package -DskipTests=true'
            }
        }
    }

    post {
        always {
            junit(testResults: 'target/surefire-reports/**/*.xml',  allowEmptyResults: true)
        }
    }
}